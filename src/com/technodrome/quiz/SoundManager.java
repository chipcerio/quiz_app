package com.technodrome.quiz;

import android.content.Context;
import android.media.MediaPlayer;
import android.text.style.BackgroundColorSpan;

public class SoundManager {
	public static MediaPlayer BACKGROUND_MUSIC = null;

	public static void play(Context context) {
		BACKGROUND_MUSIC = MediaPlayer.create(context, R.raw.sfx00);
		BACKGROUND_MUSIC.setLooping(true);
		BACKGROUND_MUSIC.start();
	}

	public static void pause() {
		BACKGROUND_MUSIC.pause();
	}

	public static void resume() {
		BACKGROUND_MUSIC.start();
	}

	public static void stop() {
		if (BACKGROUND_MUSIC != null) {
			BACKGROUND_MUSIC.stop();
			BACKGROUND_MUSIC.release();
			BACKGROUND_MUSIC = null;
		}
	}

}
