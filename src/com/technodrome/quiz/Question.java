package com.technodrome.quiz;

import com.technodrome.quiz.util.DebugLog;

public class Question {
	private String[] mQuestions;
	private String mQuestion;
	private String mChoiceA;
	private String mChoiceB;
	private String mChoiceC;
	private String mChoiceD;
	private String mAnswer;
	private String mExplanation;
	private String mMeaning;
	
	private int mTotalQuestions;
	
	private static final String TAG = "Question";
	
	public Question(String[] questions) {
		mQuestions = questions;
	}
	
	public void display(int q) {
		String[] data = mQuestions[q].split(";");
		for(int i = 0; i < data.length; i++) {
			switch (i) {
			case 0:
				// question
				setQuestion(data[0]);
				break;
				
			case 1:
				// choice a
				setChoiceA(data[1]);
				break;
				
			case 2:
				// choice b
				setChoiceB(data[2]);
				break;
				
			case 3:
				// choice c
				setChoiceC(data[3]);
				break;
				
			case 4:
				// choice d
				setChoiceD(data[4]);
				break;
				
			case 5:
				// answer
				setAnswer(data[5]);
				DebugLog.d(TAG, getAnswer());
				break;
				
			case 6:
				// explanation
				setExplanation(data[6]);
				break;
				
			case 7:
				// meaning
				setMeaning(data[7]);
				break;
				

			default:
				break;
			}
		}
	}
	
	public void setMeaning(String mMeaning) {
		this.mMeaning = mMeaning;
	}
	public String getMeaning() {
		return mMeaning;
	}
	
	public void setQuestion(String question) {
		mQuestion = question;
	}
	
	public String getQuestion() {
		return mQuestion;
	}
	
	public void setChoiceA(String choice) {
		mChoiceA = choice;
	}
	
	public String getChoiceA() {
		return mChoiceA;
	}
	
	public void setChoiceB(String choice) {
		mChoiceB = choice;
	}
	
	public String getChoiceB() {
		return mChoiceB;
	}
	
	public void setChoiceC(String choice) {
		mChoiceC = choice;
	}
	
	public String getChoiceC() {
		return mChoiceC;
	}
	
	public void setChoiceD(String choice) {
		mChoiceD = choice;
	}
	
	public String getChoiceD() {
		return mChoiceD;
	}
	
	public void setAnswer(String answer) {
		mAnswer = answer;
	}
	
	public String getAnswer() {
		return mAnswer;
	}
	
	public void setExplanation(String exp) {
		mExplanation = exp;
	}
	
	public String getExplanation() {
		return mExplanation;
	}

	public int getmTotalQuestions() {
		return mQuestions.length;
	}

	
	
}
