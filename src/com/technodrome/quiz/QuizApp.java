package com.technodrome.quiz;

import android.app.Application;
import android.content.SharedPreferences;

public class QuizApp extends Application {
	private static QuizApp mInstance;
	private static SharedPreferences mPref;
	public static boolean firstRun = true;
	
	private static final String USER_PREF = "user_pref";
	private static final String BACKGROUND_PREF = "bg_pref";

	public QuizApp() {
		super();
		mInstance = this;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	public static QuizApp getInstance() {
		if (mInstance == null) {
			synchronized (QuizApp.class) {
				if (mInstance == null)
					new QuizApp();
			}
		}
		
		if (mPref == null)
			mPref = mInstance.getSharedPreferences(USER_PREF, MODE_PRIVATE);
		
		return mInstance;
	}
	
	public void setBgMusic(boolean hasMusic) {
		SharedPreferences.Editor editor = mPref.edit();
		editor.putBoolean(BACKGROUND_PREF, hasMusic);
		editor.commit();
	}
	
	public boolean getBgMusic() {
		return mPref.getBoolean(BACKGROUND_PREF, true);
	}

}
