package com.technodrome.quiz;

import com.technodrome.quiz.util.DebugLog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class SettingsActivity extends Dialog implements OnCheckedChangeListener {
	private CheckBox mCheckBackground;
	private CheckBox mCheckSfx;
	private TextView txtSettings;
	private TextView txtBGMusic;
	private TextView txtEffectSounds;
	private Context mContext;
	public static boolean edtCheck;

	private static final String TAG = "SettingsActivity";

	public SettingsActivity(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		mContext = context;
		setCancelable(true);
		setContentView(R.layout.d_settings);
		init();
	}

	public void init() {
		SoundManager.stop();
		txtSettings = (TextView) findViewById(R.id.txtSettings);
		txtBGMusic = (TextView) findViewById(R.id.txtBGMusic);
		txtEffectSounds = (TextView) findViewById(R.id.txtEffectSounds);
		mCheckBackground = (CheckBox) findViewById(R.id.chkBackground);
		mCheckSfx = (CheckBox) findViewById(R.id.chkSound);

		mCheckBackground.setOnCheckedChangeListener(this);
		mCheckSfx.setOnCheckedChangeListener(this);
		mCheckBackground.setChecked(edtCheck);
		txtSettings.setTypeface(MainActivity.fKGTenThousandReasons);
		txtBGMusic.setTypeface(MainActivity.fKGTenThousandReasons);
		txtEffectSounds.setTypeface(MainActivity.fKGTenThousandReasons);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.chkBackground:
			if (isChecked) {
				DebugLog.d(TAG, "check background");
//				mCheckBackground.setChecked(true);
				edtCheck = true;
				SoundManager.play(mContext);
				((CheckBox) buttonView).setChecked(true);
				QuizApp.getInstance().setBgMusic(true);
			} else {
				DebugLog.d(TAG, "uncheck background");
				DebugLog.d(TAG, "uncheck background" + edtCheck);
//				mCheckBackground.setChecked(false);
				edtCheck = false;
				SoundManager.stop();
				((CheckBox) buttonView).setChecked(false);
				QuizApp.getInstance().setBgMusic(false);
			}
			break;

		case R.id.chkSound:
			if (isChecked) {
				DebugLog.d(TAG, "check sound");
			} else {
				DebugLog.d(TAG, "uncheck sound");
			}
			break;

		}

	}

}
