package com.technodrome.quiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.technodrome.quiz.util.DebugLog;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class StartActivity extends Activity {
	private String mCategory="";
	private Bundle mBundle;
	private TextView mCategoryTitle;
	private Question mQuestions;
	private TextView mMeaning;
	private TextView txtQuestions;
	private TextView mChoice1;
	private TextView mChoice2;
	private TextView mChoice3;
	private TextView mChoice4;
	private TextView mSampleTest;
	private String[] quiz;
	private boolean isVolumeOn;
	private int count = 0;
	private Button btnSounds;

	private SettingsActivity mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_start);
		mCategory = getIntent().getStringExtra("category");
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		mCategoryTitle = (TextView)findViewById(R.id.txtCategoryTitle);
		mSampleTest = (TextView)findViewById(R.id.textView2);
		mMeaning = (TextView)findViewById(R.id.txtMeaning);
		txtQuestions = (TextView)findViewById(R.id.txtSentence);
		mChoice1 = (TextView)findViewById(R.id.txtChoice1);
		mChoice2 = (TextView)findViewById(R.id.txtChoice2);
		mChoice3 = (TextView)findViewById(R.id.txtChoice3);
		mChoice4 = (TextView)findViewById(R.id.txtChoice4);
		btnSounds = (Button)findViewById(R.id.btn_start_settings);
		
		
		
		/* TODO 
		 * what category does it belong to?
		 * display the category (i.e. Adjective)
		 * display sample test about adjectives
		 * 
		 * get from string-array resources
		 * loop in the string-array and create Questions, Choices, and Answers
		 * 
		 */
		
		mBundle = new Bundle();
		
		
		mCategoryTitle.setText(mCategory);
		mCategoryTitle.setTextColor(Color.YELLOW);
		mCategoryTitle.setTypeface(MainActivity.fYellowChalk);
		
		
		if (mCategory.equalsIgnoreCase("adjectives")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_adjectives);
			mBundle.putStringArray("quiz", quiz);
			
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("adverbs")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_adverbs);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("conjunctions")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_conjunctions);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("gerunds")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_gerunds);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("prepositions")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_prepositions);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("pronouns")) {
			quiz = getResources().getStringArray(R.array.quiz_array_pronouns);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("verbs")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_verbs);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("infinitives")) {
			quiz = getResources().getStringArray(R.array.quiz_array_infinitives);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("interjections")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_interjections);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else if (mCategory.equalsIgnoreCase("nouns")) {
			 quiz = getResources().getStringArray(R.array.quiz_array_nouns);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		else   {
			quiz = getResources().getStringArray(R.array.quiz_array_participles);
			mBundle.putStringArray("quiz", quiz);
			showQuestions();
		}
		
		
	}
	
	public void settingsClick(View v) {
		// mDialog = new SettingsActivity(CategoriesActivity.this);
		// mDialog.show();
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent(this,CategoriesActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SoundManager.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		SoundManager.resume();
		super.onResume();
	}
	
	private void showQuestions() {
		mQuestions = new Question(quiz);
		mQuestions.display(0);
		txtQuestions.setText(mQuestions.getQuestion());
		mMeaning.setText(mQuestions.getMeaning());
		mChoice1.setText(mQuestions.getChoiceA());
		mChoice2.setText(mQuestions.getChoiceB());
		mChoice3.setText(mQuestions.getChoiceC());
		mChoice4.setText(mQuestions.getChoiceD());
		txtQuestions.setTypeface(MainActivity.fKGTenThousandReasons);
		mMeaning.setTypeface(MainActivity.fKGTenThousandReasons);
		mChoice1.setTypeface(MainActivity.fKGTenThousandReasons);
		mChoice2.setTypeface(MainActivity.fKGTenThousandReasons);
		mChoice3.setTypeface(MainActivity.fKGTenThousandReasons);
		mChoice4.setTypeface(MainActivity.fKGTenThousandReasons);
		mSampleTest.setTypeface(MainActivity.fKGTenThousandReasons);
		
		if (!isVolumeOn) {
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
		} else {
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
		}
		
		
		
	}
	
	public void startTheGameClick(View v) {
		Intent i = new Intent(StartActivity.this, QuizActivity.class);
		i.putExtra("category", mCategory);
		i.putExtras(mBundle);
		
		startActivity(i);
		if (mDialog != null)
			mDialog.dismiss();
		finish();
	}
	
	public void onHomeClick(View v) {
		Intent intent = new Intent(StartActivity.this,MainActivity.class);
		startActivity(intent);
		if (mDialog != null)
			mDialog.dismiss();
		finish();
	}
	
	
}
