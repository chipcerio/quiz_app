package com.technodrome.quiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.technodrome.quiz.util.DebugLog;

public class QuizActivity extends Activity {
	private TextView mQuestion;
	private Button mChoiceA;
	private Button mChoiceB;
	private Button mChoiceC;
	private Button mChoiceD;
	private int count = 0;
	private int score = 0;
	private String hint;

	private Question mQuestions;
	private String[] quiz;
	private ArrayList<Button> mButtons = new ArrayList<Button>();
	private String mCategory="";

	private static final String TAG = "QuizActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_quiz);
		/*
		 * http://bit.ly/PN5A5R Question question = new
		 * Question(string_array_from_Bundle)
		 */
		quiz = getIntent().getExtras().getStringArray("quiz");
		mCategory = getIntent().getStringExtra("category");
		
		DebugLog.d(TAG, "quiz length = " + quiz.length);
		mQuestions = new Question(quiz);
		mChoiceA = (Button) findViewById(R.id.btn_a);
		mChoiceB = (Button) findViewById(R.id.btn_b);
		mChoiceC = (Button) findViewById(R.id.btn_c);
		mChoiceD = (Button) findViewById(R.id.btn_d);
		mQuestion = (TextView) findViewById(R.id.txt_question);
	
		mButtons.add(mChoiceA);
		mButtons.add(mChoiceB);
		mButtons.add(mChoiceC);
		mButtons.add(mChoiceD);
	
		showQuestions();
		for (Button b : mButtons){
			b.setOnClickListener(mListener);
		}
				
				
	}
	
	@Override
	public void onBackPressed() {
		try{
		Intent intent = new Intent(QuizActivity.this,StartActivity.class);
		intent.putExtra("category", mCategory);
		startActivity(intent);
		finish();
		}catch(Exception e){
			e.printStackTrace();
		}
		super.onBackPressed();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SoundManager.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		SoundManager.resume();
		super.onResume();
	}
	
	private OnClickListener mListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			DebugLog.d(TAG, ((Button) v).getText().toString());
			if (count < mQuestions.getmTotalQuestions())
				for (int i = 0; i < mButtons.size(); i++) {
					if (v.getId() == mButtons.get(i).getId()) {
						if (mButtons
								.get(i)
								.getText()
								.toString()
								.trim()
								.equalsIgnoreCase(mQuestions.getAnswer().trim())) {
							score += 10;
								
							break;
						}
					}
				}
			count++;
			showQuestions();
			if (count == mQuestions.getmTotalQuestions()) {
				Intent i = new Intent(QuizActivity.this,ScoreActivity.class);
				i.putExtra("score", score);
				startActivity(i);
				finish();
				
			}
		}
	};

	private void showQuestions() {
		if (count < quiz.length) {
			mQuestions.display(count);
			hint = mQuestions.getAnswer();
			if (DebugLog.DEBUG_ON)
				Toast.makeText(this, "hint=" + hint, Toast.LENGTH_SHORT).show();
			
			List<String> randomChoices = new ArrayList<String>();
			randomChoices.add(mQuestions.getChoiceA());
			randomChoices.add(mQuestions.getChoiceB());
			randomChoices.add(mQuestions.getChoiceC());
			randomChoices.add(mQuestions.getChoiceD());
			
			Collections.shuffle(randomChoices);
			
			mQuestion.setText(mQuestions.getQuestion());
			mQuestion.setTypeface(MainActivity.fKGTenThousandReasons);
			for (int i=0;i<mButtons.size();i++){
				mButtons.get(i).setText(randomChoices.get(i));
				mButtons.get(i).setTypeface(MainActivity.fKGTenThousandReasons);
			}
			
			
			

		} else {
			if (DebugLog.DEBUG_ON)
				Toast.makeText(this, "score=" + score, Toast.LENGTH_LONG)
						.show();
		}
	}

}