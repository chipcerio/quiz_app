package com.technodrome.quiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ScoreActivity extends Activity {
	private int score;
	private TextView mFinalScore;
	private TextView mStatus;
	private TextView mYourScore;
	private Button btnHome;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_score);

		mStatus = (TextView) findViewById(R.id.txtStatus);
		mYourScore = (TextView) findViewById(R.id.txtYourScore);
		mFinalScore = (TextView) findViewById(R.id.txtFinalScore);
		
		score = getIntent().getExtras().getInt("score");
		
		mStatus.setTypeface(MainActivity.fYellowChalk);
		mStatus.setTextColor(Color.YELLOW);
		
		mYourScore.setTypeface(MainActivity.fKGTenThousandReasons);
		mYourScore.setTextColor(Color.RED);
		
		if(score < 70 ){
			mStatus.setText(R.string.try_again);
		}else if(score >= 70 && score < 80){
			mStatus.setText(R.string.fair);
		}else if(score >= 80 && score < 90){
			mStatus.setText(R.string.good);
		}else if(score >= 90 && score < 100){
			mStatus.setText(R.string.excellent);
		}else if(score == 100){
			mStatus.setText(R.string.perfect);
		}else {
			mStatus.setText(R.string.try_again);
		}
		mFinalScore.setText(""+score);
		mFinalScore.setTypeface(MainActivity.fKGTenThousandReasons);
		
	

	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SoundManager.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		SoundManager.resume();
		super.onResume();
	}

	public void onClickHome(View view){
		finish();
	}
	
	public void onClickPlayAgain(View view){
		Intent i = new Intent(ScoreActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}
}
