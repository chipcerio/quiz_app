package com.technodrome.quiz;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	public static Typeface fQuinquennial;
	public static Typeface fYellowChalk;
	public static Typeface fKGTenThousandReasons;
	protected boolean isVolumeOn = true;
	private Button btnSounds;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);
		// isVolumeOn = getIntent().getBooleanExtra("volume", true);
		// QuizApp.getInstance().setBgMusic(isVolumeOn);
		fQuinquennial = Typeface.createFromAsset(getApplicationContext()
				.getAssets(), "fonts/Quinquennial.ttf");

		fYellowChalk = Typeface.createFromAsset(getApplicationContext()
				.getAssets(), "fonts/Chalk.ttf");
		fKGTenThousandReasons = Typeface.createFromAsset(
				getApplicationContext().getAssets(),
				"fonts/KGTenThousandReasons.ttf");
		btnSounds = (Button)findViewById(R.id.btn_settings);
		if (isVolumeOn) {
			SoundManager.play(this);
		} else {
			SoundManager.stop();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		SoundManager.resume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SoundManager.pause();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	public void startClick(View v) {
		Intent i = new Intent(this, CategoriesActivity.class);
		i.putExtra("volume", isVolumeOn);
		startActivity(i);
		finish();
	}

	public void settingsClick(View v) {
		// mDialog = new SettingsActivity(MainActivity.this);
		// mDialog.show();
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}

	}

	public void exitClick(View v) {
		finish();
	}

}
