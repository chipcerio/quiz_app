package com.technodrome.quiz;

import java.util.ArrayList;

import com.technodrome.quiz.util.DebugLog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class CategoriesActivity extends Activity {

	private SettingsActivity mDialog;
	private boolean isVolumeOn;
	private ArrayList<RadioButton> mRadioButtons = new ArrayList<RadioButton>();
	private Button btnSounds;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_categories);
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		init();
		DebugLog.e("isVolume", ""+isVolumeOn);
		if (!isVolumeOn) {
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
		} else {
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
		}
		

	}

	public void letsGoClick(View v) {
		for (int i = 0; i < mRadioButtons.size(); i++) {
			if (mRadioButtons.get(i).isChecked())
				mCategory = mRadioButtons.get(i).getText().toString();
		}

		Intent i = new Intent(this, StartActivity.class);
		i.putExtra("category", mCategory);
		i.putExtra("volume", isVolumeOn);
		startActivity(i);
		finish();
	}

	public void onHomeClick(View v) {
		Intent i = new Intent(CategoriesActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

	public void settingsClick(View v) {
		// mDialog = new SettingsActivity(CategoriesActivity.this);
		// mDialog.show();
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent(this,MainActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SoundManager.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		SoundManager.resume();
		super.onResume();
	}

	// Better to use RoboGuice here
	private void init() {
	
		mGroup1 = (RadioGroup) findViewById(R.id.radio_group_1);
		mGroup2 = (RadioGroup) findViewById(R.id.radio_group_2);
		mGroup3 = (RadioGroup) findViewById(R.id.radio_group_3);

		mAdjective = (RadioButton) findViewById(R.id.radio_adjective);
		mAdverb = (RadioButton) findViewById(R.id.radio_adverb);
		mConjunctions = (RadioButton) findViewById(R.id.radio_conjunctions);
		mGerunds = (RadioButton) findViewById(R.id.radio_gerunds);
		mInfinitives = (RadioButton) findViewById(R.id.radio_infinitives);
		mInterjections = (RadioButton) findViewById(R.id.radio_interjections);
		mNouns = (RadioButton) findViewById(R.id.radio_nouns);
		mParticiples = (RadioButton) findViewById(R.id.radio_participles);
		mPrepositions = (RadioButton) findViewById(R.id.radio_prepositions);
		mPronouns = (RadioButton) findViewById(R.id.radio_pronouns);
		mVerbs = (RadioButton) findViewById(R.id.radio_verbs);
		mAdjective.setTypeface(MainActivity.fKGTenThousandReasons);
		mAdverb.setTypeface(MainActivity.fKGTenThousandReasons);
		mConjunctions.setTypeface(MainActivity.fKGTenThousandReasons);
		mGerunds.setTypeface(MainActivity.fKGTenThousandReasons);
		mInfinitives.setTypeface(MainActivity.fKGTenThousandReasons);
		mInterjections.setTypeface(MainActivity.fKGTenThousandReasons);
		mNouns.setTypeface(MainActivity.fKGTenThousandReasons);
		mParticiples.setTypeface(MainActivity.fKGTenThousandReasons);
		mPrepositions.setTypeface(MainActivity.fKGTenThousandReasons);
		mPronouns.setTypeface(MainActivity.fKGTenThousandReasons);
		mVerbs.setTypeface(MainActivity.fKGTenThousandReasons);
		btnSounds = (Button)findViewById(R.id.btn_settings2);
		
		mRadioButtons.add(mAdjective);
		mRadioButtons.add(mAdverb);
		mRadioButtons.add(mConjunctions);
		mRadioButtons.add(mGerunds);
		mRadioButtons.add(mInfinitives);
		mRadioButtons.add(mInterjections);
		mRadioButtons.add(mNouns);
		mRadioButtons.add(mParticiples);
		mRadioButtons.add(mPrepositions);
		mRadioButtons.add(mPronouns);
		mRadioButtons.add(mVerbs);
		mAdjective.setChecked(true);
		for (int i = 0; i < mRadioButtons.size(); i++) {
			mRadioButtons.get(i).setOnClickListener(mListener);
		}
	}

	private OnClickListener mListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			for (int i = 0; i < mGroup1.getChildCount(); i++) {
				if (mGroup1.getChildAt(i).getId() == v.getId()) {
					mGroup2.clearCheck();
					mGroup3.clearCheck();
					break;
				}
			}

			for (int i = 0; i < mGroup2.getChildCount(); i++) {
				if (mGroup2.getChildAt(i).getId() == v.getId()) {
					mGroup1.clearCheck();
					mGroup3.clearCheck();
					break;
				}
			}

			for (int i = 0; i < mGroup3.getChildCount(); i++) {
				if (mGroup3.getChildAt(i).getId() == v.getId()) {
					mGroup1.clearCheck();
					mGroup2.clearCheck();
					break;
				}
			}
		}
	};

	private String mCategory;
	private RadioGroup mGroup1;
	private RadioGroup mGroup2;
	private RadioGroup mGroup3;
	private RadioButton mAdjective;
	private RadioButton mAdverb;
	private RadioButton mConjunctions;
	private RadioButton mGerunds;
	private RadioButton mInfinitives;
	private RadioButton mInterjections;
	private RadioButton mNouns;
	private RadioButton mParticiples;
	private RadioButton mPrepositions;
	private RadioButton mPronouns;
	private RadioButton mVerbs;

}
